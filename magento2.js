describe('Magento 2 base tests', function () {
    it('Homepage loads', function () {
        cy.server();
        cy.visit('');
        cy.get('body').find('script[type="text/javascript"][src*="require.js"]');
    });
    it('Cart loads', function () {
        cy.server();
        cy.visit('/checkout/cart/');
        cy.get('html').find('.checkout-cart-index');
    });
});
